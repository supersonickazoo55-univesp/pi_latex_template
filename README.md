# !!! WORK IN PROGRESS !!!
This project is still a work in progress, meaning that not all specifications have been implemented or been reviewed. Use with care.

# pi_latex_template

## About
This project tries to automate the template required for reports during UNIVESP graduation program. It's mainly written in LaTeX and tries to follow ABNT specifications (and other specifications suggested in the official sample template).  
For basic LaTeX usage, follow one of the tutorials below:  
[Learn LaTeX in 30 minutes](https://www.overleaf.com/learn/latex/Learn_LaTeX_in_30_minutes)  
[A simple guide to LaTeX](https://latex-tutorial.com/tutorials/)  
[Learn LaTeX - A Beginner's Step-by-Step Guide](https://typeset.io/resources/learn-latex-beginners-step-by-step-guide/)  

## Dependencies
- texlive (command pdflatex)
- biber
- Any other package used in file [document.latex](document.latex)

## Usage
Edit the sections location in the `sections` folder with the content of your report and it should be automatically formatted once compiled. 

## Compiling document

1. Compile the document
```bash
pdflatex document.latex
```

2. Compile references/bibliography  
```bash
biber document
```

PS: The first command may need to be executed again so the table of content are correctly generated.
